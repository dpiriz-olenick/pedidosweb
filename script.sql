USE [master]
GO
/****** Object:  Database [PedidosWeb]    Script Date: 7/9/2017 10:21:02 PM ******/
CREATE DATABASE [PedidosWeb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PedidosWeb', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\PedidosWeb.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PedidosWeb_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\PedidosWeb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PedidosWeb] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PedidosWeb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PedidosWeb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PedidosWeb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PedidosWeb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PedidosWeb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PedidosWeb] SET ARITHABORT OFF 
GO
ALTER DATABASE [PedidosWeb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PedidosWeb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PedidosWeb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PedidosWeb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PedidosWeb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PedidosWeb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PedidosWeb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PedidosWeb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PedidosWeb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PedidosWeb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PedidosWeb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PedidosWeb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PedidosWeb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PedidosWeb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PedidosWeb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PedidosWeb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PedidosWeb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PedidosWeb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PedidosWeb] SET  MULTI_USER 
GO
ALTER DATABASE [PedidosWeb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PedidosWeb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PedidosWeb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PedidosWeb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [PedidosWeb]
GO
/****** Object:  Table [dbo].[Auditoria]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auditoria](
	[id_auditoria] [int] IDENTITY(1,1) NOT NULL,
	[id_empleado] [int] NOT NULL,
	[id_pedido] [int] NULL,
	[accion] [varchar](200) NULL,
	[fecha] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_auditoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Detalles]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Detalles](
	[id_detalle] [int] IDENTITY(1,1) NOT NULL,
	[id_menu] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[precio_unitario] [money] NOT NULL,
	[id_pedido] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Direcciones]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Direcciones](
	[id_direccion] [int] IDENTITY(1,1) NOT NULL,
	[direccion] [varchar](255) NOT NULL,
	[codigo_postal] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_direccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Estados]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[id_estado] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_estado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Menues]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menues](
	[id_menu] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [varchar](100) NOT NULL,
	[descripcion] [varchar](255) NOT NULL,
	[stock] [int] NULL,
	[precio] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_menu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedidos](
	[id_pedido] [int] IDENTITY(1,1) NOT NULL,
	[id_cliente] [int] NULL,
	[id_empleado] [int] NULL,
	[fecha_alta] [datetime] NULL,
	[fecha_entrega] [datetime] NULL,
	[id_estado] [int] NOT NULL,
 CONSTRAINT [PK__Pedidos__F552C2DDDEBFCA13] PRIMARY KEY CLUSTERED 
(
	[id_pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[id_rol] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [varchar](10) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[jerarquia] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_rol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Telefonos]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Telefonos](
	[id_telefono] [int] IDENTITY(1,1) NOT NULL,
	[id_tipo] [int] NOT NULL,
	[numero] [varchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_telefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tipos_Telefono]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipos_Telefono](
	[id_tipo_telefono] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_tipo_telefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [int] IDENTITY(1,1) NOT NULL,
	[apellido] [varchar](100) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[clave] [varchar](100) NOT NULL,
	[id_direccion] [int] NULL,
	[fecha_alta] [datetime] NULL,
	[fecha_baja] [datetime] NULL,
	[activo] [bit] NULL,
	[dni] [varchar](10) NOT NULL,
	[id_rol] [int] NOT NULL,
	[id_telefono] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  UserDefinedFunction [dbo].[FN_AUDITORIA]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_AUDITORIA](@DNI varchar(10) )
RETURNS TABLE
AS
RETURN (SELECT US.APELLIDO +' '+US.NOMBRE AS NOMBRE_Y_APELLIDO, COUNT(AU.ID_PEDIDO) CANTIDAD_PEDIDOS,ACCION, FECHA
 FROM AUDITORIA AU
INNER JOIN USUARIOS US ON AU.ID_EMPLEADO=US.ID_USUARIO
 WHERE US.DNI = @DNI
 GROUP BY US.APELLIDO +' '+US.NOMBRE,AU.ACCION, AU.FECHA  )
GO
/****** Object:  View [dbo].[VW_PEDIDOS_EN_DISTRIBUCION]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VW_PEDIDOS_EN_DISTRIBUCION] AS
SELECT PE.ID_PEDIDO, 
       PE.ID_MENU,
	   PE.CANTIDAD,
	   PE.FECHA_ALTA,
	   ES.DESCRIPCION AS ESTADO
FROM PEDIDOS PE INNER JOIN MENUES ME ON PE.id_menu=ME.id_menu
                INNER JOIN ESTADOS ES ON ES.id_estado=PE.id_estado
WHERE ES.descripcion = 'En distribución'


GO
/****** Object:  View [dbo].[VW_PEDIDOS_EN_PREPARACION]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_PEDIDOS_EN_PREPARACION] AS
SELECT PE.ID_PEDIDO, 
       PE.ID_MENU,
	   PE.CANTIDAD,
	   PE.FECHA_ALTA,
	   ES.DESCRIPCION AS ESTADO
FROM PEDIDOS PE INNER JOIN MENUES ME ON PE.id_menu=ME.id_menu
                INNER JOIN ESTADOS ES ON ES.id_estado=PE.id_estado
WHERE ES.descripcion = 'En preparación'


GO
/****** Object:  View [dbo].[VW_PEDIDOS_ENTREGADOS]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_PEDIDOS_ENTREGADOS] AS
SELECT PE.ID_PEDIDO, 
       PE.ID_MENU,
	   PE.CANTIDAD,
	   PE.FECHA_ALTA,
	   PE.FECHA_ENTREGA,
	   ES.DESCRIPCION AS ESTADO
FROM PEDIDOS PE INNER JOIN MENUES ME ON PE.id_menu=ME.id_menu
                INNER JOIN ESTADOS ES ON ES.id_estado=PE.id_estado
WHERE ES.descripcion = 'Entregados'


GO
/****** Object:  View [dbo].[VW_PEDIDOS_NUEVOS]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_PEDIDOS_NUEVOS] AS
SELECT PE.ID_PEDIDO, 
       PE.ID_MENU,
	   PE.CANTIDAD,
	   PE.FECHA_ALTA,
	   ES.DESCRIPCION AS ESTADO
FROM PEDIDOS PE INNER JOIN MENUES ME ON PE.id_menu=ME.id_menu
                INNER JOIN ESTADOS ES ON ES.id_estado=PE.id_estado
WHERE ES.descripcion = 'Nuevos'

GO
SET IDENTITY_INSERT [dbo].[Direcciones] ON 

INSERT [dbo].[Direcciones] ([id_direccion], [direccion], [codigo_postal]) VALUES (10, N'Triunvirato 2222', N'1430')
INSERT [dbo].[Direcciones] ([id_direccion], [direccion], [codigo_postal]) VALUES (11, N'Test1234', N'1433')
INSERT [dbo].[Direcciones] ([id_direccion], [direccion], [codigo_postal]) VALUES (12, N'Triunvirato 2222', N'1356')
INSERT [dbo].[Direcciones] ([id_direccion], [direccion], [codigo_postal]) VALUES (13, N'JDJDJDJ', N'93923')
INSERT [dbo].[Direcciones] ([id_direccion], [direccion], [codigo_postal]) VALUES (14, N'Triunvirato 2222', N'2133')
SET IDENTITY_INSERT [dbo].[Direcciones] OFF
SET IDENTITY_INSERT [dbo].[Estados] ON 

INSERT [dbo].[Estados] ([id_estado], [descripcion]) VALUES (1, N'Nuevos')
INSERT [dbo].[Estados] ([id_estado], [descripcion]) VALUES (2, N'En distribución')
INSERT [dbo].[Estados] ([id_estado], [descripcion]) VALUES (3, N'En preparación')
INSERT [dbo].[Estados] ([id_estado], [descripcion]) VALUES (4, N'Entregados')
SET IDENTITY_INSERT [dbo].[Estados] OFF
SET IDENTITY_INSERT [dbo].[Menues] ON 

INSERT [dbo].[Menues] ([id_menu], [titulo], [descripcion], [stock], [precio]) VALUES (1, N'MilaLoca Recargada', N'Milanesa de Ternera con papas fritas con cheddar y bacon', 30, 112.0000)
INSERT [dbo].[Menues] ([id_menu], [titulo], [descripcion], [stock], [precio]) VALUES (2, N'gj', N'gh', 3, 3.0000)
SET IDENTITY_INSERT [dbo].[Menues] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([id_rol], [codigo], [descripcion], [jerarquia]) VALUES (1, N'admin', N'Administrador', 0)
INSERT [dbo].[Roles] ([id_rol], [codigo], [descripcion], [jerarquia]) VALUES (2, N'emp', N'Empleado', 1)
INSERT [dbo].[Roles] ([id_rol], [codigo], [descripcion], [jerarquia]) VALUES (3, N'cli', N'Cliente', 2)
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[Telefonos] ON 

INSERT [dbo].[Telefonos] ([id_telefono], [id_tipo], [numero]) VALUES (12, 1, N'45411515')
INSERT [dbo].[Telefonos] ([id_telefono], [id_tipo], [numero]) VALUES (13, 1, N'55554443')
INSERT [dbo].[Telefonos] ([id_telefono], [id_tipo], [numero]) VALUES (14, 1, N'12341237')
INSERT [dbo].[Telefonos] ([id_telefono], [id_tipo], [numero]) VALUES (15, 1, N'1233213')
INSERT [dbo].[Telefonos] ([id_telefono], [id_tipo], [numero]) VALUES (16, 1, N'1123312323')
SET IDENTITY_INSERT [dbo].[Telefonos] OFF
SET IDENTITY_INSERT [dbo].[Tipos_Telefono] ON 

INSERT [dbo].[Tipos_Telefono] ([id_tipo_telefono], [descripcion]) VALUES (1, N'Fijo')
INSERT [dbo].[Tipos_Telefono] ([id_tipo_telefono], [descripcion]) VALUES (2, N'Celular')
INSERT [dbo].[Tipos_Telefono] ([id_tipo_telefono], [descripcion]) VALUES (3, N'Laboral')
SET IDENTITY_INSERT [dbo].[Tipos_Telefono] OFF
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([id_usuario], [apellido], [nombre], [email], [clave], [id_direccion], [fecha_alta], [fecha_baja], [activo], [dni], [id_rol], [id_telefono]) VALUES (1, N'Garcia', N'Luciano', N'lgarcia@mail.com', N'999999', 10, NULL, NULL, 1, N'17111222', 3, 12)
INSERT [dbo].[Usuarios] ([id_usuario], [apellido], [nombre], [email], [clave], [id_direccion], [fecha_alta], [fecha_baja], [activo], [dni], [id_rol], [id_telefono]) VALUES (3, N'Piriz', N'Dario', N'dpiriz@mail.com', N'778899', 11, NULL, NULL, 1, N'32152487', 1, 13)
INSERT [dbo].[Usuarios] ([id_usuario], [apellido], [nombre], [email], [clave], [id_direccion], [fecha_alta], [fecha_baja], [activo], [dni], [id_rol], [id_telefono]) VALUES (4, N'Lopez', N'Julian', N'jlopez@mail.com', N'777666', 12, NULL, NULL, 1, N'99888777', 3, 14)
INSERT [dbo].[Usuarios] ([id_usuario], [apellido], [nombre], [email], [clave], [id_direccion], [fecha_alta], [fecha_baja], [activo], [dni], [id_rol], [id_telefono]) VALUES (5, N'Test', N'User', N'mail@mail.com', N'123456', 13, NULL, NULL, 1, N'1111222', 3, 15)
INSERT [dbo].[Usuarios] ([id_usuario], [apellido], [nombre], [email], [clave], [id_direccion], [fecha_alta], [fecha_baja], [activo], [dni], [id_rol], [id_telefono]) VALUES (6, N'Test', N'User', N'mail2@mail.com', N'444333', 14, NULL, NULL, 1, N'11112224', 3, 16)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
ALTER TABLE [dbo].[Auditoria]  WITH CHECK ADD  CONSTRAINT [FK_Auditoria_Pedidos] FOREIGN KEY([id_pedido])
REFERENCES [dbo].[Pedidos] ([id_pedido])
GO
ALTER TABLE [dbo].[Auditoria] CHECK CONSTRAINT [FK_Auditoria_Pedidos]
GO
ALTER TABLE [dbo].[Auditoria]  WITH CHECK ADD  CONSTRAINT [FK_Auditoria_Usuarios] FOREIGN KEY([id_empleado])
REFERENCES [dbo].[Usuarios] ([id_usuario])
GO
ALTER TABLE [dbo].[Auditoria] CHECK CONSTRAINT [FK_Auditoria_Usuarios]
GO
ALTER TABLE [dbo].[Detalles]  WITH CHECK ADD  CONSTRAINT [FK_Detalles_Menues] FOREIGN KEY([id_menu])
REFERENCES [dbo].[Menues] ([id_menu])
GO
ALTER TABLE [dbo].[Detalles] CHECK CONSTRAINT [FK_Detalles_Menues]
GO
ALTER TABLE [dbo].[Detalles]  WITH CHECK ADD  CONSTRAINT [FK_Detalles_Pedidos] FOREIGN KEY([id_pedido])
REFERENCES [dbo].[Pedidos] ([id_pedido])
GO
ALTER TABLE [dbo].[Detalles] CHECK CONSTRAINT [FK_Detalles_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_Pedidos_Clientes] FOREIGN KEY([id_cliente])
REFERENCES [dbo].[Usuarios] ([id_usuario])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_Pedidos_Clientes]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_Pedidos_Empleados] FOREIGN KEY([id_empleado])
REFERENCES [dbo].[Usuarios] ([id_usuario])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_Pedidos_Empleados]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_Pedidos_Estados] FOREIGN KEY([id_estado])
REFERENCES [dbo].[Estados] ([id_estado])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_Pedidos_Estados]
GO
ALTER TABLE [dbo].[Telefonos]  WITH CHECK ADD  CONSTRAINT [FK_Telefonos_Tipos_Telefono] FOREIGN KEY([id_tipo])
REFERENCES [dbo].[Tipos_Telefono] ([id_tipo_telefono])
GO
ALTER TABLE [dbo].[Telefonos] CHECK CONSTRAINT [FK_Telefonos_Tipos_Telefono]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Direcciones] FOREIGN KEY([id_direccion])
REFERENCES [dbo].[Direcciones] ([id_direccion])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Direcciones]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Roles] FOREIGN KEY([id_rol])
REFERENCES [dbo].[Roles] ([id_rol])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Roles]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Telefonos] FOREIGN KEY([id_telefono])
REFERENCES [dbo].[Telefonos] ([id_telefono])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Telefonos]
GO
/****** Object:  Trigger [dbo].[TR_DEL_PEDIDOS]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TR_DEL_PEDIDOS]
ON [dbo].[Pedidos]
AFTER DELETE
      AS
BEGIN
SET NOCOUNT ON;
INSERT INTO AUDITORIA
SELECT  ID_EMPLEADO, ID_PEDIDO, 'DELETE', getdate()
         FROM DELETED
END

GO
ALTER TABLE [dbo].[Pedidos] ENABLE TRIGGER [TR_DEL_PEDIDOS]
GO
/****** Object:  Trigger [dbo].[TR_INS_PEDIDOS]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_INS_PEDIDOS]
ON [dbo].[Pedidos]
AFTER INSERT
      AS
BEGIN
SET NOCOUNT ON;
INSERT INTO AUDITORIA
SELECT  ID_EMPLEADO, ID_PEDIDO, 'INSERT', getdate()
         FROM INSERTED
END

GO
ALTER TABLE [dbo].[Pedidos] ENABLE TRIGGER [TR_INS_PEDIDOS]
GO
/****** Object:  Trigger [dbo].[TR_UPD_PEDIDOS]    Script Date: 7/9/2017 10:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TR_UPD_PEDIDOS]
ON [dbo].[Pedidos]
AFTER UPDATE
      AS
BEGIN
SET NOCOUNT ON;
INSERT INTO AUDITORIA
SELECT  ID_EMPLEADO, ID_PEDIDO, 'UPDATE', getdate()
         FROM INSERTED
END

GO
ALTER TABLE [dbo].[Pedidos] ENABLE TRIGGER [TR_UPD_PEDIDOS]
GO
USE [master]
GO
ALTER DATABASE [PedidosWeb] SET  READ_WRITE 
GO
