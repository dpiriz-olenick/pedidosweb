﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Comidator.Models;

namespace Comidator.Controllers
{
    public class Tipos_IngredienteController : Controller
    {
        private COMIDATOREntities db = new COMIDATOREntities();

        public ActionResult Index()
        {
            var tipos_Ingrediente = db.Tipos_Ingrediente.Include(t => t.Ingrediente);
            return View(tipos_Ingrediente.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipos_Ingrediente tipos_Ingrediente = db.Tipos_Ingrediente.Find(id);
            if (tipos_Ingrediente == null)
            {
                return HttpNotFound();
            }
            return View(tipos_Ingrediente);
        }

        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Ingredientes, "id", "descripcion");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tipos_Ingrediente tipos_Ingrediente)
        {
            if (ModelState.IsValid)
            {
                db.Tipos_Ingrediente.Add(tipos_Ingrediente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id = new SelectList(db.Ingredientes, "id", "descripcion", tipos_Ingrediente.id);
            return View(tipos_Ingrediente);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipos_Ingrediente tipos_Ingrediente = db.Tipos_Ingrediente.Find(id);
            if (tipos_Ingrediente == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Ingredientes, "id", "descripcion", tipos_Ingrediente.id);
            return View(tipos_Ingrediente);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tipos_Ingrediente tipos_Ingrediente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipos_Ingrediente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id = new SelectList(db.Ingredientes, "id", "descripcion", tipos_Ingrediente.id);
            return View(tipos_Ingrediente);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipos_Ingrediente tipos_Ingrediente = db.Tipos_Ingrediente.Find(id);
            if (tipos_Ingrediente == null)
            {
                return HttpNotFound();
            }
            return View(tipos_Ingrediente);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tipos_Ingrediente tipos_Ingrediente = db.Tipos_Ingrediente.Find(id);
            db.Tipos_Ingrediente.Remove(tipos_Ingrediente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
