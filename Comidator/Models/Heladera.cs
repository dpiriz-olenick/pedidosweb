//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Comidator.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Heladera
    {
        public int id { get; set; }
        public int idUsuario { get; set; }
        public int idIngrediente { get; set; }
    
        public virtual Ingrediente Ingrediente { get; set; }
    }
}
