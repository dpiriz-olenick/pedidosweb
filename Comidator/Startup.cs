﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Comidator.Startup))]
namespace Comidator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
