﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security.Provider;
using Pedidos_Web.Models;

namespace Pedidos_Web.Controllers
{
    public class HomeController : BaseController
    {
        private PedidosWebEntities db = new PedidosWebEntities();

        public ActionResult Index()
        {
            if (usuarioActual == null)
                return View("Login");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult ValidarUsuario(string usuario, string clave)
        {
            //TODO: Agregar validación para usuario activo o no

            Usuario u = db.Usuarios.FirstOrDefault(x => x.email == usuario && x.clave == clave);

            if (u == null)
            {
                ViewBag.Error = "Usuario o clave inválidos.";
                return View("Login");
            }

            if (!(u.activo ?? false))
            {
                if (u.Rol.codigo == "cli")
                {
                    //TODO: Redireccionar a la página de REACTIVACION
                    //return View("ReactivarUsuario");    
                }
                ViewBag.Error = "Su cuenta está desactivada. Contacte a su Administrador.";
                return View("Login");
            }

            if (u.clave == "123456")
            {
                Session["usuario"] = u;
                return RedirectToAction("CambiarClave", "Usuarios");
            }


            Session["usuario"] = u;
            return View("Index");
        }

        public ActionResult NuevoUsuario()
        {
            return RedirectToAction("Create", "Usuarios");
        }
        
    }
}