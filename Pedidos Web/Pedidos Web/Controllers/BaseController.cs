﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pedidos_Web.Models;

namespace Pedidos_Web.Controllers
{
    public class BaseController : Controller
    {
        protected Usuario usuarioActual;
        protected ActionExecutingContext context;

        //public BaseController()
        //{
        //    try
        //    {
        //        usuarioActual = Session["usuario"] as Usuario;
        //    }
        //    catch (NullReferenceException)
        //    {
        //        usuarioActual = null;
        //    }
            
        //}

        protected override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(this.context ?? context);
            try
            {
                usuarioActual = Session["usuario"] as Usuario;
            }
            catch (NullReferenceException)
            {
                usuarioActual = null;
            }
        }

        protected bool UsuarioLogueado()
        {
            return usuarioActual != null;
        }


        protected bool EsAdministrador()
        {
            return usuarioActual.Rol.codigo == "admin";
        }


        protected bool EsEmpleado()
        {
            return usuarioActual.Rol.codigo == "emp";
        }

        protected bool EsCliente()
        {
            return usuarioActual.Rol.codigo == "cli";
        }



    }
}