﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Pedidos_Web.Models;

namespace Pedidos_Web.Controllers
{
    public class UsuariosController : BaseController
    {
        private PedidosWebEntities db = new PedidosWebEntities();

        public ActionResult Index()
        {
            if (!UsuarioLogueado()) return RedirectToAction("Login", "Home");
            if (!EsAdministrador()) return RedirectToAction("Index", "Home");
            IEnumerable<Usuario> usuarios = db.Usuarios;
            return View(usuarios.ToList());
        }
        
        public ActionResult Create()
        {
            if (UsuarioLogueado() && !EsAdministrador()) RedirectToAction("Index", "Home");
            Usuario usuario = new Usuario();
            usuario.activo = true;
            usuario.clave = "123456";

            ViewBag.id_tipo_telefono = new SelectList(db.Tipos_Telefono, "id_tipo_telefono", "descripcion");
            ViewBag.id_rol = new SelectList(db.Rols.Where(x => x.codigo != "cli"), "id_rol", "descripcion");
            return View(usuario);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Usuario usuario)
        {
            //Validaciones
            int duplicados = 0;
            duplicados = db.Usuarios.Count(x => x.dni == usuario.dni);

            if (duplicados > 0)
                ModelState.AddModelError("dni", "El DNI ingresado ya existe en nuestro sistema.");

            duplicados = db.Usuarios.Count(x => x.email == usuario.email);
            if (duplicados > 0)
                ModelState.AddModelError("email", "El email ingresado ya existe en nuestro sistema.");

            int idTipo = Convert.ToInt32(Request.Form["id_tipo_telefono"]);
            string numero = Request.Form["numero"];

            if (ModelState.IsValid)
            {
                
                Direccion d = new Direccion()
                {
                    codigo_postal = Request.Form["codigo_postal"],
                    direccion = Request.Form["_direccion"]
                };

                Telefono t = new Telefono()
                {
                    id_tipo = idTipo,
                    numero = numero
                };

                usuario.Direccion = d;
                usuario.Telefono = t;

                Rol rol = db.Rols.FirstOrDefault(x => x.codigo == "cli");
                usuario.Rol = rol;

                db.Usuarios.Add(usuario);
                db.SaveChanges();

                
                if (!UsuarioLogueado())
                    return RedirectToAction("Login", "Home");
                return RedirectToAction("Index");
            }

            ViewBag.id_tipo_telefono = new SelectList(db.Tipos_Telefono, "id_tipo_telefono", "descripcion");
            ViewBag.id_rol = new SelectList(db.Rols.Where(x => x.codigo != "cli"), "id_rol", "descripcion");

            return View(usuario);
        }

        public ActionResult Edit(int? id)
        {
            if (!UsuarioLogueado()) return RedirectToAction("Login", "Home");
            if (!EsAdministrador() && id != usuarioActual.id_usuario) return RedirectToAction("Index", "Home");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_tipo_telefono = new SelectList(db.Tipos_Telefono, "id_tipo_telefono", "descripcion", usuario.Telefono.id_tipo);
            ViewBag.id_rol = new SelectList(db.Rols.Where(x => x.codigo != "cli"), "id_rol", "descripcion", usuario.Rol.id_rol);
            ViewBag._direccion = usuario.Direccion.direccion;
            ViewBag.codigo_postal = usuario.Direccion.codigo_postal;
            ViewBag.numero = usuario.Telefono.numero;

            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Usuario usuario)
        {
            Usuario u = db.Usuarios.Find(usuario.id_usuario);
            Direccion d = db.Direccions.Find(u.id_direccion);
            Telefono t = db.Telefonos.Find(u.id_telefono);

            d.codigo_postal = Request.Form["codigo_postal"];
            d.direccion = Request.Form["_direccion"];

            if (EsAdministrador())
            {
                u.activo = usuario.activo;
                u.id_rol = usuario.id_rol;
            }

            db.Entry(d).State = EntityState.Modified;

            int idTipo = Convert.ToInt32(Request.Form["id_tipo_telefono"]);
            string numero = Request.Form["numero"];

            t.id_tipo = idTipo;
            t.numero = numero;
            db.Entry(t).State = EntityState.Modified;

            db.SaveChanges();
            return RedirectToAction("Index", "Home");
            
        }

        public ActionResult CambiarClave()
        {
            if (!UsuarioLogueado()) return RedirectToAction("Login", "Home");
            Usuario u = db.Usuarios.FirstOrDefault(x => x.dni == usuarioActual.dni);
            return View(u);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CambiarClave(Usuario u)
        {
            Usuario _u = db.Usuarios.FirstOrDefault(x => x.dni == usuarioActual.dni);
            _u.clave = Request.Form["nueva_clave"];
            db.Entry(_u).State = EntityState.Modified;
            db.SaveChanges();

            ViewBag.Error = "¡Contraseña actualizada correctamente!";
            return RedirectToAction("Login", "Home");
        }

        public ActionResult Logout()
        {
            if (!UsuarioLogueado()) return RedirectToAction("Login", "Home");
            Session.Remove("usuario");
            return RedirectToAction("Login", "Home");
        }

        public ActionResult EditarPerfil()
        {
            if (!UsuarioLogueado()) return RedirectToAction("Login", "Home");
            return RedirectToAction("Edit", new {id = usuarioActual.id_usuario});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
