﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pedidos_Web.Models;

namespace Pedidos_Web.Controllers
{
    public class PedidosController : BaseController
    {
        private PedidosWebEntities db = new PedidosWebEntities();
        
        // GET: Pedidos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pedido pedidos = db.Pedidos.Find(id);
            if (pedidos == null)
            {
                return HttpNotFound();
            }
            return View(pedidos);
        }

        // GET: Pedidos/Create
        public ActionResult Create()
        {
            Estado e = db.Estados.Find(1); //Pedido nuevo
            Pedido nuevoPedido = new Pedido()
            {
                Cliente = usuarioActual,
                Estado = e
            };


            Session["pedido"] = nuevoPedido;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id_Menu, Descripcion, precio, cantidad")] Menue menu)
        public ActionResult Create([Bind(Include = "Id_Menu, Cantidad")] Menu menu)
        {

            string cantidad = Request.Form["cant"];
            string id_menu = Request.Form["id_menu"];

            //Validaciones
            //que el ID exista: ir a la base y traer el menu cuyo ID sea el que viene por post
            Menu _menu = db.Menus.Find(Convert.ToInt16(id_menu));
            if (_menu == null)
            {
                return RedirectToAction("Create");
                //return HttpNotFound();
            }

            //que el numero sea > 0
            int cant = Int16.Parse(cantidad);
            if (cant <= 0)
            {
                return RedirectToAction("Create");

            }

            //que haya suficiente stock
            //_menu.stock >= cantidad
            if (cant > _menu.stock)
            {
                return RedirectToAction("Create");
            }

            Pedido p = new Pedido();
            //Cargar el nuevo objeto pedido
            //p.id_menu = _menu.id_menu;
            //            p.precio_unitario = _menu.precio;
            //p.cantidad = cant;
            //p.id_cliente = 
            //p.id_empleado = 
            //          p.fecha_alta = DateTime;
            p.fecha_entrega = null;
            //p.id_estado = 0;

            if (ModelState.IsValid)
            {
                db.Pedidos.Add(p);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(menu);

        }


        // GET: Pedidos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pedido pedido = db.Pedidos.Find(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_estado = new SelectList(db.Estados, "id_estado", "descripcion", pedido.id_estado);
            //ViewBag.id_menu = new SelectList(db.Menus, "id_menu", "titulo", pedido.id_menu);
            ViewBag.id_cliente = new SelectList(db.Usuarios, "id_usuario", "apellido", pedido.id_cliente);
            ViewBag.id_empleado = new SelectList(db.Usuarios, "id_usuario", "apellido", pedido.id_empleado);
            return View(pedido);
        }

        // POST: Pedidos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_pedido,id_menu,precio_unitario,cantidad,id_cliente,id_empleado,fecha_alta,fecha_entrega,id_estado")] Pedido pedido)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pedido).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            ViewBag.id_estado = new SelectList(db.Estados, "id_estado", "descripcion", pedido.id_estado);
            //ViewBag.id_menu = new SelectList(db.Menus, "id_menu", "titulo", pedido.id_menu);
            ViewBag.id_cliente = new SelectList(db.Usuarios, "id_usuario", "apellido", pedido.id_cliente);
            ViewBag.id_empleado = new SelectList(db.Usuarios, "id_usuario", "apellido", pedido.id_empleado);
            return View(pedido);
        }

        [HttpPost]
        public JsonResult ActualizarPedido()
        {
            bool resultado = true;
            string mensaje = "";

            string id_menu = Request.Form["id_menu"];
            string cantidad = Request.Form["cant"];

            Menu m = db.Menus.Find(id_menu);

            //Stock > 0
            if (Convert.ToInt64(cantidad) < 1)
            {
                resultado = false;
                mensaje = "La cantidad ingresada es invalida.";
                return Json(new { message = mensaje, success = resultado });
            }

            //Validar si hay stock
            if (m.stock < Convert.ToInt64(cantidad))
            {
                resultado = false;
                mensaje = "No hay suficiente stock para abastecer el pedido del menú seleccionado.";
                return Json(new { message = mensaje, success = resultado });
            }

            Pedido p = Session["pedido"] as Pedido;
            Detalle d = new Detalle()
            {
                id_menu = m.id_menu,
                cantidad = Convert.ToInt32(cantidad),
                precio_unitario = m.precio ?? 0
            };
            



            return Json(new { message = "", success = true });
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
