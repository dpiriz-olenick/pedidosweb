﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Pedidos_Web.Models;

namespace Pedidos_Web.Controllers
{
    public class MenuesController : BaseController
    {
        private PedidosWebEntities db = new PedidosWebEntities();

        // GET: Menues
        public ActionResult Index()
        {
            if (!UsuarioLogueado()) RedirectToAction("Login", "Home");
            if (EsCliente()) RedirectToAction("Index", "Home");
            return View(db.Menus.ToList());
        }
        
        // GET: Menues/Create
        public ActionResult Create()
        {
            if (!UsuarioLogueado()) RedirectToAction("Login", "Home");
            if (EsCliente()) RedirectToAction("Index", "Home");
            return View();
        }

        // POST: Menues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_menu,titulo,descripcion,stock,precio")] Menu menu)
        {
            if (ModelState.IsValid)
            {
                db.Menus.Add(menu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(menu);
        }

        // GET: Menues/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!UsuarioLogueado()) return RedirectToAction("Login", "Home");
            if (EsCliente()) return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menus.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }
            return View(menu);
        }

        // POST: Menues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_menu,titulo,descripcion,stock,precio")] Menu menue)
        {
            if (ModelState.IsValid)
            {
                db.Entry(menue).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(menue);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}