﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pedidos_Web.Startup))]
namespace Pedidos_Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
